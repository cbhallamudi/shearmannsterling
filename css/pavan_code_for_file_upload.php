<?php
date_default_timezone_set("Asia/Kolkata");
define("KEY", "exampleKey"); 
define("SITE_URL","http://exampleWebServer.com"); 
header("Content-Type:application/json");
 
if (!isset($_REQUEST['key']) || trim($_REQUEST['key']) != KEY) {
    die("Not authorised");
}
 
if (!isset($_REQUEST['action'])) {
    echo "Please provide action parameter";
    die();
}
 
$action = trim($_REQUEST['action']);
 
if ($action == 'upload') {
 
    $upload_path = 'uploads/';
    $upload_url = SITE_URL . '/' . $upload_path;   

    if (isset($_FILES['file']['name'])) {                                   
        $fileinfo = pathinfo($_FILES['file']['name']);            
        $extension = $fileinfo['extension'];            
        $filename = time();
        $file_url = $upload_url . $filename . '.' . $extension;
 
        $file_path = $_SERVER["DOCUMENT_ROOT"]."/".$upload_path . $filename . '.' . $extension;            
        try {
            if(move_uploaded_file($_FILES['file']['tmp_name'], $file_path)) {
                $data['fileurl'] = $file_url;
                $data['filetype'] = $extension;
                respond("200", "Uploaded successfully", $data);
            }                
        } catch (Exception $e) {
            respond("201", $e->getMessage(), null);
        }            
    } else {
        respond("201", 'File not selected', null);
    }    
 
}
 
function respond($status, $status_message, $data) {
    header("HTTP/1.1 $status $status_message");
    $response['status'] = $status;
    $response['status_message'] = $status_message;
    $response['data'] = $data;
 
    echo json_encode($response);
}
?>